﻿using Dios.Lib.BL;
using Dios.Lib.Dal;
using Dios.Lib.Dto;
using Dios.Lib.Model;
using Moq;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Dios.Test.BL
{

    public class JadwalBLTest
    {
        private readonly IJadwalBL _sut;

        private readonly Mock<IJadwalDal> _mockJadwalDal;
        private readonly Mock<IIbadahTypeDal> _mockIbadahTypeDal;
        private readonly Mock<INunaCounterBL> _mockNunaCounter;

        public JadwalBLTest()
        {
            _mockJadwalDal = new Mock<IJadwalDal>();
            _mockIbadahTypeDal = new Mock<IIbadahTypeDal>();
            _mockNunaCounter = new Mock<INunaCounterBL>();
            _sut = new JadwalBL(_mockJadwalDal.Object,
                _mockIbadahTypeDal.Object, _mockNunaCounter.Object);
        }

        private JadwalAddDto TestDataDto() =>
            new JadwalAddDto
            {
                JadwalName = "A2",
                IbadahTypeID = "A3",
                Hari = 1,
                Jam = "08:00"
            };


        [Fact]
        public void Add_EmptyJadwalName_ThrowArgEx()
        {
            //      ARRANGE
            var tdDto = TestDataDto();
            tdDto.JadwalName = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Add(tdDto));
        }
        [Fact]
        public void Add_EmptyIbadahTypeID_ThrowArgEx()
        {
            //      ARRANGE
            var tdDto = TestDataDto();
            tdDto.IbadahTypeID = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Add(tdDto));
        }
        [Fact]
        public void Add_InvalidIbadahTypeID_ThrowEx()
        {
            //      ARRANGE
            var tdDto = TestDataDto();
            _mockIbadahTypeDal.Setup(x => x
                .GetData(It.IsAny<IIbadahTypeKey>()))
                .Returns(null as IbadahTypeModel);

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Add(tdDto));
        }


        [Fact]
        public void Update_EmptyJadwalName_ThrowArgEx()
        {
            //      ARRANGE
            var tdDto = TestDataDto();
            var key = new JadwalModel { JadwalID = "A1" };
            tdDto.JadwalName = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Update(key, tdDto));
        }
        [Fact]
        public void Update_EmptyIbadahTypeID_ThrowArgEx()
        {
            //      ARRANGE
            var tdDto = TestDataDto();
            var key = new JadwalModel { JadwalID = "A1" };
            tdDto.IbadahTypeID = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Add(tdDto));
        }
        [Fact]
        public void Update_InvalidIbadahTypeID_ThrowEx()
        {
            //      ARRANGE
            var tdDto = TestDataDto();
            var key = new JadwalModel { JadwalID = "A1" };
            _mockIbadahTypeDal.Setup(x => x
                .GetData(It.IsAny<IIbadahTypeKey>()))
                .Returns(null as IbadahTypeModel);

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Update(key, tdDto));
        }
        [Fact]
        public void Update_InvalidKey_ThrowEx()
        {
            //      ARRANGE
            var tdDto = TestDataDto();
            var key = new JadwalModel { JadwalID = "A1" };
            _mockJadwalDal.Setup(x => x
                .GetData(It.IsAny<IJadwalKey>()))
                .Returns(null as JadwalModel);

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Update(key, tdDto));
        }

    }

}
