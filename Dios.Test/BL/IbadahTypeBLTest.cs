﻿using Dios.Lib.BL;
using Dios.Lib.Dal;
using Dios.Lib.Model;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Dios.Test.BL
{

    public class IbadahTypeBLTest
    {
        private readonly IIbadahTypeBL _sut;
        private readonly Mock<IIbadahTypeDal> _mockIbadahTypeDal;

        public IbadahTypeBLTest()
        {
            _mockIbadahTypeDal = new Mock<IIbadahTypeDal>();
            _sut = new IbadahTypeBL(_mockIbadahTypeDal.Object);
        }

        private IbadahTypeModel TestData() =>
            new IbadahTypeModel
            {
                IbadahTypeID = "A1",
                IbadahTypeName = "A2"
            };

        [Fact]
        public void Save_EmptyIbadahTypeID_ThrowArgEx()
        {
            //      ARRANGE
            var td = TestData();
            td.IbadahTypeID = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Save(td));
        }

        [Fact]
        public void Save_EmptyIbadahTypeName_ThrowArgEx()
        {
            //      ARRANGE
            var td = TestData();
            td.IbadahTypeName = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Save(td));
        }

        [Fact]
        public void Save_NewValidData_CallInsert()
        {
            //      ARRANGE
            _mockIbadahTypeDal.Setup(x => x
                .GetData(It.IsAny<IIbadahTypeKey>()))
                .Returns(null as IbadahTypeModel);

            //      ACT
            var actual = _sut.Save(TestData());

            //      ASSERT
            _mockIbadahTypeDal.Verify(x => x
                .Insert(It.IsAny<IbadahTypeModel>()));
        }

        [Fact]
        public void Save_ExistValidData_CallUpdate()
        {
            //      ARRANGE
            _mockIbadahTypeDal.Setup(x => x
                .GetData(It.IsAny<IIbadahTypeKey>()))
                .Returns(new IbadahTypeModel());

            //      ACT
            var actual = _sut.Save(TestData());

            //      ASSERT
            _mockIbadahTypeDal.Verify(x => x
                .Update(It.IsAny<IbadahTypeModel>()));
        }

        [Fact]
        public void Delete_Test()
        {
            //      ACT
            _sut.Delete(TestData());

            //      ASSERT
            _mockIbadahTypeDal.Verify(x => x
                .Delete(It.IsAny<IIbadahTypeKey>()));
        }

        [Fact]
        public void GetData_Test()
        {
            //      ACT
            var actual = _sut.GetData(TestData());

            //      ASSERT
            //var p = It.IsAny<IbadahTypeModel>();
            _mockIbadahTypeDal.Verify(x => x
                .GetData(It.IsAny<IbadahTypeModel>()));

        }

        [Fact]
        public void ListData_Test()
        {
            //      ACT
            var actual = _sut.ListData();

            //      ASSERT
            _mockIbadahTypeDal.Verify(x => x.ListData());
        }
    }
}
