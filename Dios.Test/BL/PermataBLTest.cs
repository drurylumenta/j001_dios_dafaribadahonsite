﻿using Dios.Lib.Model;
using Dios.Lib.Dal;
using Dios.Lib.BL;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Xunit;

namespace Dios.Test.BL
{

    public class PermataBLTest
    {
        private readonly IPermataBL _sut;
        private readonly Mock<IPermataDal> _mockPermataDal;

        public PermataBLTest()
        {
            _mockPermataDal = new Mock<IPermataDal>();
            _sut = new PermataBL(_mockPermataDal.Object);
        }

        private PermataModel TestData() =>
            new PermataModel
            {
                PermataID = "A1",
                PermataName = "A2"
            };

        [Fact]
        public void Save_EmptyPermataID_ThrowArgEx()
        {
            //      ARRANGE
            var td = TestData();
            td.PermataID = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Save(td));
        }

        [Fact]
        public void Save_EmptyPermataName_ThrowArgEx()
        {
            //      ARRANGE
            var td = TestData();
            td.PermataName = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Save(td));
        }

        [Fact]
        public void Save_NewValidData_CallInsert()
        {
            //      ARRANGE
            _mockPermataDal.Setup(x => x
                .GetData(It.IsAny<IPermataKey>()))
                .Returns(null as PermataModel);

            //      ACT
            var actual = _sut.Save(TestData());

            //      ASSERT
            _mockPermataDal.Verify(x => x
                .Insert(It.IsAny<PermataModel>()));
        }

        [Fact]
        public void Save_ExistValidData_CallUpdate()
        {
            //      ARRANGE
            _mockPermataDal.Setup(x => x
                .GetData(It.IsAny<IPermataKey>()))
                .Returns(new PermataModel());

            //      ACT
            var actual = _sut.Save(TestData());

            //      ASSERT
            _mockPermataDal.Verify(x => x
                .Update(It.IsAny<PermataModel>()));
        }

        [Fact]
        public void Delete_Test()
        {
            //      ACT
            _sut.Delete(TestData());

            //      ASSERT
            _mockPermataDal.Verify(x => x
                .Delete(It.IsAny<IPermataKey>()));
        }

        [Fact]
        public void GetData_Test()
        {
            //      ACT
            var actual = _sut.GetData(TestData());

            //      ASSERT
            //var p = It.IsAny<PermataModel>();
            _mockPermataDal.Verify(x => x
                .GetData(It.IsAny<PermataModel>()));

        }

        [Fact]
        public void ListData_Test()
        {
            //      ACT
            var actual = _sut.ListData();

            //      ASSERT
            _mockPermataDal.Verify(x => x.ListData());
        }
    }
}
