﻿using Dios.Lib.BL;
using Dios.Lib.Dal;
using Dios.Lib.Model;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Dios.Test.BL
{

    public class UserrBLTest
    {
        private readonly IUserrBL _sut;
        private readonly Mock<IUserrDal> _mockUserrDal;

        public UserrBLTest()
        {
            _mockUserrDal = new Mock<IUserrDal>();
            _sut = new UserrBL(_mockUserrDal.Object);
        }

        private UserrModel TestData() =>
            new UserrModel
            {
                UserrID = "A1",
                DeviceToken = "A2",
                UserrLevel = UserrLevelEnum.Admin
            };

        [Fact]
        public void Save_EmptyUserrID_ThrowArgEx()
        {
            //      ARRANGE
            var td = TestData();
            td.UserrID = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Save(td));
        }

        [Fact]
        public void Save_EmptyDeviceToken_ThrowArgEx()
        {
            //      ARRANGE
            var td = TestData();
            td.DeviceToken = string.Empty;

            //      ACT - ASSERT
            var ex = Assert.Throws<ArgumentException>(
                () => _sut.Save(td));
        }

        [Fact]
        public void Save_NewValidData_CallInsert()
        {
            //      ARRANGE
            _mockUserrDal.Setup(x => x
                .GetData(It.IsAny<IUserrKey>()))
                .Returns(null as UserrModel);

            //      ACT
            var actual = _sut.Save(TestData());

            //      ASSERT
            _mockUserrDal.Verify(x => x
                .Insert(It.IsAny<UserrModel>()));
        }

        [Fact]
        public void Save_ExistValidData_CallUpdate()
        {
            //      ARRANGE
            _mockUserrDal.Setup(x => x
                .GetData(It.IsAny<IUserrKey>()))
                .Returns(new UserrModel());

            //      ACT
            var actual = _sut.Save(TestData());

            //      ASSERT
            _mockUserrDal.Verify(x => x
                .Update(It.IsAny<UserrModel>()));
        }

        [Fact]
        public void Delete_Test()
        {
            //      ACT
            _sut.Delete(TestData());

            //      ASSERT
            _mockUserrDal.Verify(x => x
                .Delete(It.IsAny<IUserrKey>()));
        }

        [Fact]
        public void GetData_Test()
        {
            //      ACT
            var actual = _sut.GetData(TestData());

            //      ASSERT
            //var p = It.IsAny<UserrModel>();
            _mockUserrDal.Verify(x => x
                .GetData(It.IsAny<UserrModel>()));

        }

        [Fact]
        public void ListData_Test()
        {
            //      ACT
            var actual = _sut.ListData();

            //      ASSERT
            _mockUserrDal.Verify(x => x.ListData());
        }
    }
}
