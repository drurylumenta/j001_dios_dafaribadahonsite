﻿using Dios.Lib.Model;
using Dios.Lib.Dal;
using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using Xunit;
using Nuna.Lib.Helper;

namespace Dios.Test.Dal
{

    public class IbadahTypeDalTest
    {
        private readonly IIbadahTypeDal _sut;

        public IbadahTypeDalTest()
        {
            _sut = new IbadahTypeDal();
        }

        private IbadahTypeModel TestData()
        {
            var result = new IbadahTypeModel
            {
                IbadahTypeID = "A1",
                IbadahTypeName = "A2"
            };
            return result;
        }

        [Fact]
        public void Insert_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Insert(expected);
            }
        }

        [Fact]
        public void Update_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Update(expected);
            }
        }

        [Fact]
        public void Delete_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Delete(expected);
            }
        }
        [Fact]
        public void GetData_Found_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();
                _sut.Insert(expected);

                //  act
                var actual = _sut.GetData(expected);

                //  assert
                actual.Should().BeEquivalentTo(expected);
            }
        }

        [Fact]
        public void GetData_NotFound_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act
                var actual = _sut.GetData(expected);

                //  assert
                actual.Should().BeNull();
            }
        }


        [Fact]
        public void ListData_Found_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = new List<IbadahTypeModel> { TestData() };
                _sut.Insert(TestData());

                //  act
                var actual = _sut.ListData();

                //  assert
                actual.Should().BeEquivalentTo(expected);
            }
        }

        [Fact]
        public void ListData_NotFound_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange

                //  act
                var actual = _sut.ListData();

                //  assert
                actual.Should().BeNull();
            }
        }
    }
}
