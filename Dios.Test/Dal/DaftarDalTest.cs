﻿using Dios.Lib.Dal;
using Dios.Lib.Model;
using FluentAssertions;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Dios.Test.Dal
{

    public class DaftarDalTest
    {
        private readonly IDaftarDal _sut;

        public DaftarDalTest()
        {
            _sut = new DaftarDal();
        }

        private DaftarModel TestData()
        {
            var result = new DaftarModel
            {
                DaftarID = "A1",
                TglJam = new DateTime(2020, 7, 18, 9, 0, 0),
                UserrID = "A2",
                UserrName = "",

                IbadahID = "A3",
                TglJamIbadah=new DateTime(1900,1,1,0,0,0),
                PesertaName = "A4",
                JumPerson = 3,
                Keterangan = "A5",
                PermataID = "A6",
                PermataName= ""
            };
            return result;
        }

        [Fact]
        public void Insert_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Insert(expected);
            }
        }

        [Fact]
        public void Update_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Update(expected);
            }
        }

        [Fact]
        public void Delete_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Delete(expected);
            }
        }
        [Fact]
        public void GetData_Found_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();
                _sut.Insert(expected);

                //  act
                var actual = _sut.GetData(expected);

                //  assert
                actual.Should().BeEquivalentTo(expected);
            }
        }

        [Fact]
        public void GetData_NotFound_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act
                var actual = _sut.GetData(expected);

                //  assert
                actual.Should().BeNull();
            }
        }


        [Fact]
        public void ListData_Found_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = new List<DaftarModel> { TestData() };
                _sut.Insert(TestData());

                //  act
                var actual = _sut.ListData(TestData());

                //  assert
                actual.Should().BeEquivalentTo(expected);
            }
        }

        [Fact]
        public void ListData_NotFound_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange

                //  act
                var actual = _sut.ListData(TestData());

                //  assert
                actual.Should().BeNull();
            }
        }
    }
}