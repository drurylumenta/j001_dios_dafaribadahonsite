﻿using Dios.Lib.Dal;
using Dios.Lib.Model;
using FluentAssertions;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Dios.Test.Dal
{

    public class JadwalDalTest
    {
        private readonly IJadwalDal _sut;

        public JadwalDalTest()
        {
            _sut = new JadwalDal();
        }

        private JadwalModel TestData()
        {
            var result = new JadwalModel
            {
                JadwalID = "A1",
                JadwalName = "A2",
                IbadahTypeID = "A3",
                IbadahTypeName = "",
                Hari = DayOfWeek.Monday,
                Jam = "09:00"
            };
            return result;
        }

        [Fact]
        public void Insert_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Insert(expected);
            }
        }

        [Fact]
        public void Update_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Update(expected);
            }
        }

        [Fact]
        public void Delete_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act-assert
                _sut.Delete(expected);
            }
        }
        [Fact]
        public void GetData_Found_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();
                _sut.Insert(expected);

                //  act
                var actual = _sut.GetData(expected);

                //  assert
                actual.Should().BeEquivalentTo(expected);
            }
        }

        [Fact]
        public void GetData_NotFound_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = TestData();

                //  act
                var actual = _sut.GetData(expected);

                //  assert
                actual.Should().BeNull();
            }
        }


        [Fact]
        public void ListData_Found_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange
                var expected = new List<JadwalModel> { TestData() };
                _sut.Insert(TestData());

                //  act
                var actual = _sut.ListData();

                //  assert
                actual.Should().BeEquivalentTo(expected);
            }
        }

        [Fact]
        public void ListData_NotFound_Test()
        {
            using (var trans = TransHelper.NewScope())
            {
                //  arrange

                //  act
                var actual = _sut.ListData();

                //  assert
                actual.Should().BeNull();
            }
        }
    }
}
