﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dios.Lib.Dal
{

    public interface IUserrDal :
        IInsert<UserrModel>,
        IUpdate<UserrModel>,
        IDelete<IUserrKey>,
        IGetData<UserrModel, IUserrKey>,
        IListBrowse<UserrModel>
        { }

    public class UserrDal : IUserrDal
    {
        public void Insert(UserrModel model)
        {
            var sql = @"
                INSERT INTO 
                    DIOS_Userr (
                        UserrID, UserrName, DeviceToken, UserrLevel )
                VALUES (
                        @UserrID, @UserrName, @DeviceToken, @UserrLevel) ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@UserrID", model.UserrID, SqlDbType.VarChar);
                cmd.AddParam("@UserrName", model.UserrName, SqlDbType.VarChar);
                cmd.AddParam("@DeviceToken", model.DeviceToken, SqlDbType.VarChar);
                cmd.AddParam("@UserrLEvel", model.UserrLevel, SqlDbType.Int);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(UserrModel model)
        {
            var sql = @"
                UPDATE
                    DIOS_Userr 
                SET
                    UserrName = @UserrName, 
                    DeviceToken = @DeviceToken,
                    UserrLevel = @UserrLevel
                WHERE
                    @UserrID = UserrID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@UserrID", model.UserrID, SqlDbType.VarChar);
                cmd.AddParam("@UserrName", model.UserrName, SqlDbType.VarChar);
                cmd.AddParam("@DeviceToken", model.DeviceToken, SqlDbType.VarChar);
                cmd.AddParam("@UserrLevel", model.UserrLevel, SqlDbType.Int);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(IUserrKey key)
        {
            var sql = @"
                DELETE
                    DIOS_Userr 
                WHERE
                    @UserrID = UserrID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@UserrID", key.UserrID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public UserrModel GetData(IUserrKey key)
        {
            UserrModel result = null;
            var sql = @"
                SELECT
                    UserrName, UserrID, 
                    DeviceToken, UserrLevel
                FROM
                    DIOS_Userr
                WHERE
                    UserrID = @UserrID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@UserrID", key.UserrID, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    dr.Read();
                    result = new UserrModel
                    {
                        UserrID = dr["UserrID"].ToString(),
                        UserrName = dr["UserrName"].ToString(),
                        DeviceToken = dr["DeviceToken"].ToString(),
                        UserrLevel = (UserrLevelEnum)Convert.ToInt16(dr["UserrLevel"])
                    };
                }
            }
            return result;
        }

        public IEnumerable<UserrModel> ListData()
        {
            List<UserrModel> result = null;
            var sql = @"
                SELECT
                    UserrID, UserrName, DeviceToken, UserrLevel
                FROM
                    DIOS_Userr ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    result = new List<UserrModel>();
                    while (dr.Read())
                    {
                        var item = new UserrModel
                        {
                            UserrID = dr["UserrID"].ToString(),
                            UserrName = dr["UserrName"].ToString(),
                            DeviceToken = dr["DeviceToken"].ToString(),
                            UserrLevel = (UserrLevelEnum)Convert.ToInt16(dr["UserrLevel"])
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }
    }
}
