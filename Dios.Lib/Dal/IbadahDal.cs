﻿using Dios.Lib.Model;
using Dios.Lib.Dal;
using System;
using System.Collections.Generic;
using System.Text;
using Nuna.Lib.Helper;
using System.Data.SqlClient;
using System.Data;

namespace Dios.Lib.Dal
{

    public interface IIbadahDal :
        IInsert<IbadahModel>,
        IUpdate<IbadahModel>,
        IDelete<IIbadahKey>,
        IGetData<IbadahModel, IIbadahKey>,
        IListPeriode<IbadahModel>
    { }

    public class IbadahDal : IIbadahDal
    {
        public void Insert(IbadahModel ibadah)
        {
            var sql = @"
                INSERT INTO 
                    DIOS_Ibadah (
                        IbadahID, IbadahTypeID, 
                        TglJam, Kapasitas, Daftar)
                VALUES (
                        @IbadahID, @IbadahTypeID, 
                        @TglJam, @Kapasitas, @Daftar) ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahID", ibadah.IbadahID, SqlDbType.VarChar);
                cmd.AddParam("@IbadahTypeID", ibadah.IbadahTypeID, SqlDbType.VarChar);
                
                cmd.AddParam("@TglJam", ibadah.TglJam, SqlDbType.DateTime);
                
                cmd.AddParam("@Kapasitas", ibadah.Kapasitas, SqlDbType.Int);
                cmd.AddParam("@Daftar", ibadah.Daftar, SqlDbType.Int);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(IbadahModel ibadah)
        {
            var sql = @"
                UPDATE
                    DIOS_Ibadah 
                SET
                    IbadahTypeID = @IbadahTypeID,
                    TglJam = @TglJam,
                    Kapasitas = @Kapasitas,
                    Daftar = @Daftar
                WHERE
                    @IbadahID = IbadahID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahID", ibadah.IbadahID, SqlDbType.VarChar);
                cmd.AddParam("@IbadahTypeID", ibadah.IbadahTypeID, SqlDbType.VarChar);
                cmd.AddParam("@TglJam", ibadah.TglJam, SqlDbType.DateTime);
                cmd.AddParam("@Kapasitas", ibadah.Kapasitas, SqlDbType.Int);
                cmd.AddParam("@Daftar", ibadah.Daftar, SqlDbType.Int);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(IIbadahKey ibadah)
        {
            var sql = @"
                DELETE
                    DIOS_Ibadah 
                WHERE
                    @IbadahID = IbadahID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahID", ibadah.IbadahID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public IbadahModel GetData(IIbadahKey ibadah)
        {
            IbadahModel result = null;
            var sql = @"
                SELECT
                    aa.IbadahID, aa.IbadahTypeID
                    ,aa.TglJam, aa.Kapasitas, aa.Daftar
                    ,ISNULL(bb.IbadahTypeName, '') IbadahTypeName
                FROM
                    DIOS_Ibadah aa
                    LEFT JOIN DIOS_IbadahType bb ON aa.IbadahTypeID = bb.IbadahTypeID
                WHERE
                    IbadahID = @IbadahID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahID", ibadah.IbadahID, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    dr.Read();
                    result = new IbadahModel
                    {
                        IbadahID = dr["IbadahID"].ToString(),
                        IbadahTypeID = dr["IbadahTypeID"].ToString(),
                        TglJam = Convert.ToDateTime(dr["TglJam"]),
                        Kapasitas = Convert.ToInt16(dr["Kapasitas"]),
                        Daftar = Convert.ToInt16(dr["Daftar"])
                    };
                }
            }
            return result;
        }

        public IEnumerable<IbadahModel> ListData(NunaPeriode periode)
        {
            List<IbadahModel> result = null;
            var sql = @"
                SELECT
                    aa.IbadahID, aa.IbadahTypeID
                    ,aa.TglJam, aa.Kapasitas, aa.Daftar
                    ,ISNULL(bb.IbadahTypeName, '') IbadahTypeName
                FROM
                    DIOS_Ibadah aa
                    LEFT JOIN DIOS_IbadahType bb ON aa.IbadahTypeID = bb.IbadahTypeID
                WHERE
                    aa.TglJam BETWEEN @TglJam1 AND @TglJam2 ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@TglJam1", periode.TglAwal, SqlDbType.VarChar);
                cmd.AddParam("@TglJam2", periode.TglAkhir, SqlDbType.VarChar);

                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    result = new List<IbadahModel>();
                    while (dr.Read())
                    {
                        var item = new IbadahModel
                        {
                            IbadahID = dr["IbadahID"].ToString(),
                            IbadahTypeID = dr["IbadahTypeID"].ToString(),
                            IbadahTypeName = dr["IbadahTypeName"].ToString(),
                            TglJam = Convert.ToDateTime(dr["TglJam"]),
                            Kapasitas = Convert.ToInt16(dr["Kapasitas"]),
                            Daftar = Convert.ToInt16(dr["Daftar"])
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }
    }
}
