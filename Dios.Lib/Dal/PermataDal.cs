﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dios.Lib.Dal
{

    public interface IPermataDal :
        IInsert<PermataModel>,
        IUpdate<PermataModel>,
        IDelete<IPermataKey>,
        IGetData<PermataModel, IPermataKey>,
        IListBrowse<PermataModel>
        { }

    public class PermataDal : IPermataDal
    {
        public void Insert(PermataModel model)
        {
            var sql = @"
                INSERT INTO 
                    DIOS_Permata (
                        PermataID, PermataName )
                VALUES (
                        @PermataID, @PermataName) ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@PermataID", model.PermataID, SqlDbType.VarChar);
                cmd.AddParam("@PermataName", model.PermataName, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(PermataModel model)
        {
            var sql = @"
                UPDATE
                    DIOS_Permata 
                SET
                    PermataName = @PermataName 
                WHERE
                    @PermataID = PermataID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@PermataID", model.PermataID, SqlDbType.VarChar);
                cmd.AddParam("@PermataName", model.PermataName, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(IPermataKey key)
        {
            var sql = @"
                DELETE
                    DIOS_Permata 
                WHERE
                    @PermataID = PermataID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@PermataID", key.PermataID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public PermataModel GetData(IPermataKey key)
        {
            PermataModel result = null;
            var sql = @"
                SELECT
                    PermataID, PermataName
                FROM
                    DIOS_Permata
                WHERE
                    PermataID = @PermataID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@PermataID", key.PermataID, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    dr.Read();
                    result = new PermataModel
                    {
                        PermataID = dr["PermataID"].ToString(),
                        PermataName = dr["PermataName"].ToString()
                    };
                }
            }
            return result;
        }

        public IEnumerable<PermataModel> ListData()
        {
            List<PermataModel> result = null;
            var sql = @"
                SELECT
                    PermataID, PermataName
                FROM
                    DIOS_Permata ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    result = new List<PermataModel>();
                    while (dr.Read())
                    {
                        var item = new PermataModel
                        {
                            PermataID = dr["PermataID"].ToString(),
                            PermataName = dr["PermataName"].ToString()
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }
    }
}
