﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dios.Lib.Dal
{

    public interface IParamNoDal :
        IInsert<ParamNoModel>,
        IUpdate<ParamNoModel>,
        IDelete<IParamNoKey>,
        IGetData<ParamNoModel, IParamNoKey>,
        IListBrowse<ParamNoModel>,
        INunaCounterDal
    { }

    public class ParamNoDal : IParamNoDal
    {
        public void Insert(ParamNoModel model)
        {
            var sql = @"
                INSERT INTO 
                    DIOS_ParamNo (
                        Prefix, PValue )
                VALUES (
                        @Prefix, @PValue) ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@Prefix", model.Prefix, SqlDbType.VarChar);
                cmd.AddParam("@PValue", model.PValue, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(ParamNoModel model)
        {
            var sql = @"
                UPDATE
                    DIOS_ParamNo 
                SET
                    PValue = @PValue 
                WHERE
                    @Prefix = Prefix ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@Prefix", model.Prefix, SqlDbType.VarChar);
                cmd.AddParam("@PValue", model.PValue, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(IParamNoKey key)
        {
            var sql = @"
                DELETE
                    DIOS_ParamNo 
                WHERE
                    @Prefix = Prefix ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@Prefix", key.Prefix, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public ParamNoModel GetData(IParamNoKey key)
        {
            ParamNoModel result = null;
            var sql = @"
                SELECT
                    Prefix, PValue
                FROM
                    DIOS_ParamNo
                WHERE
                    Prefix = @Prefix ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@Prefix", key.Prefix, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    dr.Read();
                    result = new ParamNoModel
                    {
                        Prefix = dr["Prefix"].ToString(),
                        PValue = dr["PValue"].ToString()
                    };
                }
            }
            return result;
        }

        public IEnumerable<ParamNoModel> ListData()
        {
            List<ParamNoModel> result = null;
            var sql = @"
                SELECT
                    Prefix, PValue
                FROM
                    DIOS_ParamNo ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    result = new List<ParamNoModel>();
                    while (dr.Read())
                    {
                        var item = new ParamNoModel
                        {
                            Prefix = dr["Prefix"].ToString(),
                            PValue = dr["PValue"].ToString()
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public string GetNewHexNumber(string prefix)
        {
            var p = GetData(new ParamNoModel { Prefix = prefix });
            if (p is null)
                return null;
            return p.PValue;
        }

        public void UpdateNewHexNumber(string prefix, string hexValue)
        {
            var p = new ParamNoModel
            {
                Prefix = prefix,
                PValue = hexValue
            };
            Update(p);
        }

        public void InsertNewHexNumber(string prefix, string hexValue)
        {
            var p = new ParamNoModel
            {
                Prefix = prefix,
                PValue = hexValue
            };
            Insert(p);
        }
    }
}
