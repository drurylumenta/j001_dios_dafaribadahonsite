﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dios.Lib.Dal
{

    public interface IJadwalDal :
        IInsert<JadwalModel>,
        IUpdate<JadwalModel>,
        IDelete<IJadwalKey>,
        IGetData<JadwalModel, IJadwalKey>,
        IListBrowse<JadwalModel>
    { }

    public class JadwalDal : IJadwalDal
    {
        public void Insert(JadwalModel jadwal)
        {
            var sql = @"
                INSERT INTO 
                    DIOS_Jadwal (
                        JadwalID, JadwalName, IbadahTypeID, 
                        Hari, Jam)
                VALUES (
                        @JadwalID, @JadwalName, @IbadahTypeID, 
                        @Hari, @Jam) ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@JadwalID", jadwal.JadwalID, SqlDbType.VarChar);
                cmd.AddParam("@JadwalName", jadwal.JadwalName, SqlDbType.VarChar);
                cmd.AddParam("@IbadahTypeID", jadwal.IbadahTypeID, SqlDbType.VarChar);
                cmd.AddParam("@Hari", jadwal.Hari, SqlDbType.Int);
                cmd.AddParam("@Jam", jadwal.Jam, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(JadwalModel jadwal)
        {
            var sql = @"
                UPDATE
                    DIOS_Jadwal 
                SET
                    JadwalName = @JadwalName,
                    IbadahTypeID = @IbadahTypeID,
                    Hari = @Hari,
                    Jam = @Jam
                WHERE
                    @JadwalID = JadwalID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@JadwalID", jadwal.JadwalID, SqlDbType.VarChar);
                cmd.AddParam("@JadwalName", jadwal.JadwalName, SqlDbType.VarChar);
                cmd.AddParam("@IbadahTypeID", jadwal.IbadahTypeID, SqlDbType.VarChar);
                cmd.AddParam("@Hari", jadwal.Hari, SqlDbType.Int);
                cmd.AddParam("@Jam", jadwal.Jam, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(IJadwalKey key)
        {
            var sql = @"
                DELETE
                    DIOS_Jadwal 
                WHERE
                    @JadwalID = JadwalID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@JadwalID", key.JadwalID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public JadwalModel GetData(IJadwalKey key)
        {
            JadwalModel result = null;
            var sql = @"
                SELECT
                    aa.JadwalID, aa.JadwalName, aa.IbadahTypeID
                    ,aa.Hari, aa.Jam
                    ,ISNULL(bb.IbadahTypeName, '') IbadahTypeName
                FROM
                    DIOS_Jadwal aa
                    LEFT JOIN DIOS_IbadahType bb On aa.IbadahTypeID = bb.IbadahTypeName
                WHERE
                    JadwalID = @JadwalID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@JadwalID", key.JadwalID, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    dr.Read();
                    result = new JadwalModel
                    {
                        JadwalID = dr["JadwalID"].ToString(),
                        JadwalName = dr["JadwalName"].ToString(),
                        IbadahTypeID = dr["IbadahTypeID"].ToString(),
                        IbadahTypeName = dr["IbadahTypeName"].ToString(),
                        Hari = (DayOfWeek)Convert.ToInt16(dr["Hari"]),
                        Jam = dr["Jam"].ToString()
                    };
                }
            }
            return result;
        }

        public IEnumerable<JadwalModel> ListData()
        {
            List<JadwalModel> result = null;
            var sql = @"
                SELECT
                    aa.JadwalID, aa.JadwalName, aa.IbadahTypeID
                    ,aa.Hari, aa.Jam
                    ,ISNULL(bb.IbadahTypeName, '') IbadahTypeName
                FROM
                    DIOS_Jadwal aa
                    LEFT JOIN DIOS_IbadahType bb On aa.IbadahTypeID = bb.IbadahTypeName ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    result = new List<JadwalModel>();
                    while (dr.Read())
                    {
                        var item = new JadwalModel
                        {
                            JadwalID = dr["JadwalID"].ToString(),
                            JadwalName = dr["JadwalName"].ToString(),
                            IbadahTypeID = dr["IbadahTypeID"].ToString(),
                            IbadahTypeName = dr["IbadahTypeName"].ToString(),
                            Hari = (DayOfWeek)Convert.ToInt16(dr["Hari"]),
                            Jam = dr["Jam"].ToString()
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }
    }
}
