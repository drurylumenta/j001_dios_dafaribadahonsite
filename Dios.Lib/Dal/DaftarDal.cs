﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dios.Lib.Dal
{

    public interface IDaftarDal :
        IInsert<DaftarModel>,
        IUpdate<DaftarModel>,
        IDelete<IDaftarKey>,
        IGetData<DaftarModel, IDaftarKey>,
        IListFilter<DaftarModel, IIbadahKey>
        { }

    public class DaftarDal : IDaftarDal
    {
        public void Insert(DaftarModel daftar)
        {
            var sql = @"
                INSERT INTO 
                    DIOS_Daftar (
                        DaftarID, TglJam, UserrID, IbadahID
                        ,PesertaName, JumPerson, Keterangan
                        ,PermataID)
                VALUES (
                        @DaftarID, @TglJam, @UserrID, @IbadahID,
                        @PesertaName, @JumPerson, @Keterangan,
                        @PermataID) ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@DaftarID", daftar.DaftarID, SqlDbType.VarChar);
                cmd.AddParam("@TglJam", daftar.TglJam, SqlDbType.DateTime);
                cmd.AddParam("@UserrID", daftar.UserrID, SqlDbType.VarChar);
                cmd.AddParam("@IbadahID", daftar.IbadahID, SqlDbType.VarChar);

                cmd.AddParam("@PesertaName", daftar.PesertaName, SqlDbType.VarChar);
                cmd.AddParam("@JumPerson", daftar.JumPerson, SqlDbType.VarChar);
                cmd.AddParam("@Keterangan", daftar.Keterangan, SqlDbType.VarChar);
                cmd.AddParam("@PermataID", daftar.PermataID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(DaftarModel daftar)
        {
            var sql = @"
                UPDATE
                    DIOS_Daftar 
                SET
                    TglJam = @TglJam
                    ,UserrID = @UserrID
                    ,IbadahID = @IbadahID
                    ,PesertaName = @PesertaName 
                    ,JumPerson = @JumPerson 
                    ,Keterangan = @Keterangan 
                    ,PermataID = @PermataID 
                WHERE
                    @DaftarID = DaftarID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@DaftarID", daftar.DaftarID, SqlDbType.VarChar);
                cmd.AddParam("@TglJam", daftar.TglJam, SqlDbType.DateTime);
                cmd.AddParam("@UserrID", daftar.UserrID, SqlDbType.VarChar);
                cmd.AddParam("@IbadahID", daftar.IbadahID, SqlDbType.VarChar);
                cmd.AddParam("@PesertaName", daftar.PesertaName, SqlDbType.VarChar);
                cmd.AddParam("@JumPerson", daftar.JumPerson, SqlDbType.VarChar);
                cmd.AddParam("@Keterangan", daftar.Keterangan, SqlDbType.VarChar);
                cmd.AddParam("@PermataID", daftar.PermataID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(IDaftarKey key)
        {
            var sql = @"
                DELETE
                    DIOS_Daftar 
                WHERE
                    @DaftarID = DaftarID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@DaftarID", key.DaftarID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public DaftarModel GetData(IDaftarKey key)
        {
            DaftarModel result = null;
            var sql = @"
                SELECT
                    aa.DaftarID, aa.TglJam, aa.UserrID
                    ,aa.IbadahID, aa.PesertaName, aa.JumPerson
                    ,aa.Keterangan, aa.PermataID
                    ,ISNULL(bb.IbadahTypeID, '') IbadahTypeID
                    ,ISNULL(bb.TglJam, '1900-01-01T00:00:00') TglJamIbadah
                    ,ISNULL(bb1.IbadahTypeName, '') IbadahTypeName
                    ,ISNULL(dd.PermataName, '') PermataName
                    ,ISNULL(ee.UserrName, '') UserrName
                FROM
                    DIOS_Daftar aa
                    LEFT JOIN DIOS_Ibadah bb ON aa.IbadahID = bb.IbadahID
                    LEFT JOIN DIOS_IbadahType bb1 ON bb.IbadahTypeID = bb1.IbadahTypeID
                    LEFT JOIN DIOS_Permata dd ON aa.PermataID = dd.PermataID
                    LEFT JOIN DIOS_Userr ee ON aa.UserrID = ee.UserrID
                WHERE
                    DaftarID = @DaftarID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@DaftarID", key.DaftarID, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    dr.Read();
                    result = new DaftarModel
                    {
                        DaftarID = dr["DaftarID"].ToString(),
                        TglJam = Convert.ToDateTime(dr["TglJam"]),
                        UserrID = dr["UserrID"].ToString(),
                        UserrName = dr["UserrName"].ToString(),

                        IbadahID = dr["IbadahID"].ToString(),
                        IbadahTypeID = dr["IbadahTypeID"].ToString(),
                        IbadahTypeName = dr["IbadahTypeName"].ToString(),
                        TglJamIbadah = Convert.ToDateTime(dr["TglJamIbadah"]),
                        
                        PesertaName = dr["PesertaName"].ToString(),
                        JumPerson = Convert.ToInt16(dr["JumPerson"]),
                        Keterangan = dr["Keterangan"].ToString(),
                        PermataID = dr["PermataID"].ToString(),
                        PermataName = dr["PermataName"].ToString(),
                    };
                }
            }
            return result;
        }

        public IEnumerable<DaftarModel> ListData(IIbadahKey ibadah)
        {
            List<DaftarModel> result = null;
            var sql = @"
                SELECT
                    aa.DaftarID, aa.TglJam, aa.UserrID
                    ,aa.IbadahID, aa.PesertaName, aa.JumPerson
                    ,aa.Keterangan, aa.PermataID
                    ,ISNULL(bb.IbadahTypeID, '') IbadahTypeID
                    ,ISNULL(bb.TglJam, '1900-01-01T00:00:00') TglJamIbadah
                    ,ISNULL(bb1.IbadahTypeName, '') IbadahTypeName
                    ,ISNULL(dd.PermataName, '') PermataName
                    ,ISNULL(ee.UserrName, '') UserrName
                FROM
                    DIOS_Daftar aa
                    LEFT JOIN DIOS_Ibadah bb ON aa.IbadahID = bb.IbadahID
                    LEFT JOIN DIOS_IbadahType bb1 ON bb.IbadahTypeID = bb1.IbadahTypeID
                    LEFT JOIN DIOS_Permata dd ON aa.PermataID = dd.PermataID
                    LEFT JOIN DIOS_Userr ee ON aa.UserrID = ee.UserrID 
                WHERE
                    aa.IbadahID = @IbadahID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahID", ibadah.IbadahID, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    result = new List<DaftarModel>();
                    while (dr.Read())
                    {
                        var item = new DaftarModel
                        {
                            DaftarID = dr["DaftarID"].ToString(),
                            TglJam = Convert.ToDateTime(dr["TglJam"]),
                            UserrID = dr["UserrID"].ToString(),
                            UserrName = dr["UserrName"].ToString(),

                            IbadahID = dr["IbadahID"].ToString(),
                            IbadahTypeID = dr["IbadahTypeID"].ToString(),
                            IbadahTypeName = dr["IbadahTypeName"].ToString(),
                            TglJamIbadah = Convert.ToDateTime(dr["TglJamIbadah"]),

                            PesertaName = dr["PesertaName"].ToString(),
                            JumPerson = Convert.ToInt16(dr["JumPerson"]),
                            Keterangan = dr["Keterangan"].ToString(),
                            PermataID = dr["PermataID"].ToString(),
                            PermataName = dr["PermataName"].ToString(),
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }
    }
}
