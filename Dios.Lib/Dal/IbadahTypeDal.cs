﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dios.Lib.Dal
{

    public interface IIbadahTypeDal :
        IInsert<IbadahTypeModel>,
        IUpdate<IbadahTypeModel>,
        IDelete<IIbadahTypeKey>,
        IGetData<IbadahTypeModel, IIbadahTypeKey>,
        IListBrowse<IbadahTypeModel>
        { }

    public class IbadahTypeDal : IIbadahTypeDal
    {
        public void Insert(IbadahTypeModel model)
        {
            var sql = @"
                INSERT INTO 
                    DIOS_IbadahType (
                        IbadahTypeID, IbadahTypeName )
                VALUES (
                        @IbadahTypeID, @IbadahTypeName) ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahTypeID", model.IbadahTypeID, SqlDbType.VarChar);
                cmd.AddParam("@IbadahTypeName", model.IbadahTypeName, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Update(IbadahTypeModel model)
        {
            var sql = @"
                UPDATE
                    DIOS_IbadahType 
                SET
                    IbadahTypeName = @IbadahTypeName 
                WHERE
                    @IbadahTypeID = IbadahTypeID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahTypeID", model.IbadahTypeID, SqlDbType.VarChar);
                cmd.AddParam("@IbadahTypeName", model.IbadahTypeName, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(IIbadahTypeKey key)
        {
            var sql = @"
                DELETE
                    DIOS_IbadahType 
                WHERE
                    @IbadahTypeID = IbadahTypeID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahTypeID", key.IbadahTypeID, SqlDbType.VarChar);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public IbadahTypeModel GetData(IIbadahTypeKey key)
        {
            IbadahTypeModel result = null;
            var sql = @"
                SELECT
                    IbadahTypeID, IbadahTypeName
                FROM
                    DIOS_IbadahType
                WHERE
                    IbadahTypeID = @IbadahTypeID ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                cmd.AddParam("@IbadahTypeID", key.IbadahTypeID, SqlDbType.VarChar);
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    dr.Read();
                    result = new IbadahTypeModel
                    {
                        IbadahTypeID = dr["IbadahTypeID"].ToString(),
                        IbadahTypeName = dr["IbadahTypeName"].ToString()
                    };
                }
            }
            return result;
        }

        public IEnumerable<IbadahTypeModel> ListData()
        {
            List<IbadahTypeModel> result = null;
            var sql = @"
                SELECT
                    IbadahTypeID, IbadahTypeName
                FROM
                    DIOS_IbadahType ";
            using (var conn = new SqlConnection(ConnStringHelper.Get()))
            using (var cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.HasRows)
                        return null;
                    result = new List<IbadahTypeModel>();
                    while (dr.Read())
                    {
                        var item = new IbadahTypeModel
                        {
                            IbadahTypeID = dr["IbadahTypeID"].ToString(),
                            IbadahTypeName = dr["IbadahTypeName"].ToString()
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }
    }
}
