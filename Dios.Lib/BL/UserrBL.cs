﻿using Dios.Lib.Dal;
using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.BL
{

    public interface IUserrBL
    {
        UserrModel Save(UserrModel userr);
        void Delete(IUserrKey userr);
        UserrModel GetData(IUserrKey userr);
        IEnumerable<UserrModel> ListData();
    }

    public class UserrBL : IUserrBL
    {
        private readonly IUserrDal _userrDal;

        public UserrBL(IUserrDal userrDal)
        {
            _userrDal = userrDal;
        }

        public UserrModel Save(UserrModel userr)
        {
            //      VALIDATE ARGUMENT
            userr.Empty().Throw("Data Userr kosong");
            userr.UserrID.Empty().Throw("Kode Userr kosong");
            userr.DeviceToken.Empty().Throw("Device Token kosong");

            //      APPLY REPO
            var us = _userrDal.GetData(userr);
            if (us is null)
                _userrDal.Insert(userr);
            else
                _userrDal.Update(userr);

            //      SET RETURN
            return userr;
        }

        public void Delete(IUserrKey userr)
        {
            _userrDal.Delete(userr);
        }

        public UserrModel GetData(IUserrKey userr)
        {
            return _userrDal.GetData(userr);
        }

        public IEnumerable<UserrModel> ListData()
        {
            return _userrDal.ListData();
        }
    }
}
