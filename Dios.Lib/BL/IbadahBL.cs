﻿using Dios.Lib.Dal;
using Dios.Lib.Dto;
using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dios.Lib.BL
{
    public interface IIbadahBL
    {
        IbadahModel Add(IbadahAddDto ibadah);

        void Delete(IIbadahKey ibadah);

        IbadahModel GetData(IIbadahKey ibadah);

        IEnumerable<IbadahModel> ListData(NunaPeriode periode);

        IbadahModel UbahJadwal(string tgl, string jam);

        IbadahModel Update(IbadahUpdateDto ibadah);
    }

    public class IbadahBL : IIbadahBL
    {
        private readonly IIbadahDal _ibadahDal;
        private readonly IIbadahTypeDal _ibadahTypeDal;
        private readonly IJadwalDal _jadwalDal;
        private readonly INunaCounterBL _nunaCounterBL;

        public IbadahBL(IIbadahDal ibadahDal,
            IIbadahTypeDal ibadahTypeDal,
            IJadwalDal jadwalDal,
            INunaCounterBL nunaCounterBL)
        {
            _ibadahDal = ibadahDal;
            _ibadahTypeDal = ibadahTypeDal;
            _jadwalDal = jadwalDal;
            _nunaCounterBL = nunaCounterBL;
        }

        public IbadahModel Add(IbadahAddDto ibadah)
        {
            //      VALIDATE ARGUMENT
            var result = Validate(ibadah);
            IsDuplicated(result).Throw("Duplikasi data ibadah");

            //      APPLY REPO
            using (var trans = TransHelper.NewScope())
            {
                result.IbadahID = _nunaCounterBL.Generate("IB", IDFormatEnum.Prefix_YYM_3C);
                _ibadahDal.Insert(result);
                trans.Complete();
            }

            return result;
        }

        public void Delete(IIbadahKey ibadah)
        {
            _ibadahDal.Delete(ibadah);
        }

        public IbadahModel GetData(IIbadahKey ibadah)
        {
            return _ibadahDal.GetData(ibadah);
        }

        public IEnumerable<IbadahModel> ListData(NunaPeriode periode)
        {
            return _ibadahDal.ListData(periode);
        }

        public IbadahModel UbahJadwal(string tgl, string jam)
        {
            throw new NotImplementedException();
        }

        public IbadahModel Update(IbadahUpdateDto ibadah)
        {
            //      VALIDATE ARGUMENT
            var result = Validate(ibadah);
            result.IbadahID = ibadah.IbadahID;

            //      CONSTRUCT MODEL
            var ibadahDb = _ibadahDal.GetData(ibadah);
            ibadahDb.Empty().Throw("IbadahID tidak ditemukan");
            result.Daftar = ibadahDb.Daftar;
            
            //      APPLY REPO
            using (var trans = TransHelper.NewScope())
            {
                _ibadahDal.Update(result);
            }
            return result;
        }


        #region PRIVATE FUNCTION

        private bool IsDuplicated(IbadahModel ibadah)
        {
            //  duplication check; IbadahType + Tgl + Jam

            var listIbadah = _ibadahDal.ListData(new NunaPeriode(ibadah.TglJam, ibadah.TglJam));
            if (listIbadah is null)
                return false;

            var exist = listIbadah
                .Where(x => x.TglJam == ibadah.TglJam)
                .FirstOrDefault(x => x.IbadahTypeID == ibadah.IbadahTypeID);
            if (exist is null)
                return false;
            else
                return true;
        }

        private IbadahModel Validate(IbadahAddDto ibadah)
        {
            //  basic validation
            ibadah.Empty().Throw("Data Ibadah kosong");
            ibadah.IbadahTypeID.Empty().Throw("Jadwal Ibadah kosong");
            ibadah.Kapasitas.LessOrEqual(0).Throw("Kapasitas ibadah kosong");
            
            ibadah.Tgl.IsInvalidTgl("yyyy-MM-dd").Throw("Tanggal invalid");
            ibadah.Jam.IsInvalidJam("HH:mm").Throw("Jam invalid");

            //  dto >> model
            var result = (IbadahModel)ibadah;

            //  advance validation
            var ibdType = _ibadahTypeDal.GetData(result);
            ibdType.Empty().Throw("Ibadah Type invalid");
            result.IbadahTypeName = ibdType.IbadahTypeName;

            return result;
        }

        #endregion
    }
}