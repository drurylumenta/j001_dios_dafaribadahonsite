﻿using Dios.Lib.Model;
using Dios.Lib.Dal;
using System;
using System.Collections.Generic;
using System.Text;
using Nuna.Lib.Helper;

namespace Dios.Lib.BL
{

    public interface IPermataBL
    {
        PermataModel Save(PermataModel permata);
        void Delete(IPermataKey permata);
        PermataModel GetData(IPermataKey permata);
        IEnumerable<PermataModel> ListData();
    }

    public class PermataBL : IPermataBL
    {
        private readonly IPermataDal _permataDal;

        public PermataBL(IPermataDal permataDal)
        {
            _permataDal = permataDal;
        }

        public PermataModel Save(PermataModel permata)
        {
            //      VALIDATE ARGUMENT
            permata.Empty().Throw("Data Permata kosong");
            permata.PermataID.Empty().Throw("Kode Permata kosong");
            permata.PermataName.Empty().Throw("Nama Permata kosong");

            //      APPLY REPO
            var prmt = _permataDal.GetData(permata);
            if (prmt is null)
                _permataDal.Insert(permata);
            else
                _permataDal.Update(permata);

            //      SET RETURN
            return permata;
        }

        public void Delete(IPermataKey permata)
        {
            _permataDal.Delete(permata);
        }

        public PermataModel GetData(IPermataKey permata)
        {
            return _permataDal.GetData(permata);
        }

        public IEnumerable<PermataModel> ListData()
        {
            return _permataDal.ListData();
        }
    }
}
