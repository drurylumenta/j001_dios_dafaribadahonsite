﻿using Dios.Lib.Dal;
using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.BL
{

    public interface IIbadahTypeBL
    {
        IbadahTypeModel Save(IbadahTypeModel ibadahType);
        void Delete(IIbadahTypeKey ibadahType);
        IbadahTypeModel GetData(IIbadahTypeKey ibadahType);
        IEnumerable<IbadahTypeModel> ListData();
    }

    public class IbadahTypeBL : IIbadahTypeBL
    {
        private readonly IIbadahTypeDal _ibadahTypeDal;

        public IbadahTypeBL(IIbadahTypeDal ibadahTypeDal)
        {
            _ibadahTypeDal = ibadahTypeDal;
        }

        public IbadahTypeModel Save(IbadahTypeModel ibadahType)
        {
            //      VALIDATE ARGUMENT
            ibadahType.Empty().Throw("Data IbadahType kosong");
            ibadahType.IbadahTypeID.Empty().Throw("Kode IbadahType kosong");
            ibadahType.IbadahTypeName.Empty().Throw("Nama IbadahType kosong");

            //      APPLY REPO
            var it = _ibadahTypeDal.GetData(ibadahType);
            if (it is null)
                _ibadahTypeDal.Insert(ibadahType);
            else
                _ibadahTypeDal.Update(ibadahType);

            //      SET RETURN
            return ibadahType;
        }

        public void Delete(IIbadahTypeKey ibadahType)
        {
            _ibadahTypeDal.Delete(ibadahType);
        }

        public IbadahTypeModel GetData(IIbadahTypeKey ibadahType)
        {
            return _ibadahTypeDal.GetData(ibadahType);
        }

        public IEnumerable<IbadahTypeModel> ListData()
        {
            return _ibadahTypeDal.ListData();
        }
    }
}
