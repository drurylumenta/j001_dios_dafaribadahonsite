﻿using Dios.Lib.Dal;
using Dios.Lib.Dto;
using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.BL
{

    public interface IDaftarBL
    {
        DaftarModel Add(DaftarAddDto daftar);
        DaftarModel Update(DaftarUpdateDto daftar);
        void Delete(IDaftarKey daftar);
        DaftarModel GetData(IDaftarKey daftar);
        IEnumerable<DaftarModel> ListData(IIbadahKey ibadah);
    }

    public class DaftarBL : IDaftarBL
    {
        private readonly IDaftarDal _daftarDal;
        private readonly IIbadahDal _ibadahDal;
        private readonly INunaCounterBL _nunaCounterBL;
        private readonly IPermataDal _permataDal;
        private readonly IUserrDal _userrDal;
        public DaftarBL(IDaftarDal daftarDal,
            IPermataDal permataDal,
            IIbadahDal ibadahDal,
            IUserrDal userrDal,
            INunaCounterBL nunaCounterBL)
        {
            _daftarDal = daftarDal;
            _permataDal = permataDal;
            _ibadahDal = ibadahDal;
            _userrDal = userrDal;
            _nunaCounterBL = nunaCounterBL;
        }

        public DaftarModel Add(DaftarAddDto daftar)
        {
            //      VALIDATE ARGUMENT
            var result = Validate(daftar);

            //      CONSTRUCT REPO
            result.TglJam = DateTime.Now;

            //      APPLY REPO
            using (var trans = TransHelper.NewScope())
            {
                result.DaftarID = _nunaCounterBL.Generate("DF", IDFormatEnum.Prefix_YYM_3_3C);
                _daftarDal.Insert(result);
                trans.Complete();
            }

            //      SET RETURN
            return result;
        }

        public void Delete(IDaftarKey daftar)
        {
            _daftarDal.Delete(daftar);
        }

        public DaftarModel GetData(IDaftarKey daftar)
        {
            return _daftarDal.GetData(daftar);
        }

        public IEnumerable<DaftarModel> ListData(IIbadahKey ibadah)
        {
            return _daftarDal.ListData(ibadah);
        }

        public DaftarModel Update(DaftarUpdateDto daftar)
        {
            //      VALIDATE ARGUMENT
            var result = Validate(daftar);
            result.DaftarID = daftar.IbadahID;

            //      CONSTRUCT MODEL
            var daftarDb = _daftarDal.GetData(daftar);
            daftarDb.Empty().Throw("DaftarID tidak ditemukan");

            //      APPLY REPO
            using (var trans = TransHelper.NewScope())
            {
                _daftarDal.Update(result);
            }
            return result;
        }

        #region PRIVATE FUNCTION

        private DaftarModel Validate(DaftarAddDto daftar)
        {
            //      BASIC VALIDATION
            daftar.Empty().Throw("Data Daftar kosong");
            daftar.IbadahID.Empty().Throw("Ibadah ID kosong");
            daftar.PesertaName.Empty().Throw("Peserta Name kosong");
            daftar.JumPerson.LessOrEqual(0).Throw("Jumlah Person kosong");

            //      DTO >> MODEL
            var result = (DaftarModel)daftar;


            //      ADVANCE VALIDATION
            var ibadah = _ibadahDal.GetData(result);
            ibadah.Empty().Throw("Ibadah ID invalid");
            result.IbadahTypeID = ibadah.IbadahTypeID;
            result.IbadahTypeName = ibadah.IbadahTypeName;
            result.TglJamIbadah = ibadah.TglJam;

            var userr = _userrDal.GetData(result);
            userr.Empty().Throw("Userr ID invalid");
            result.UserrName = userr.UserrName;

            result.PermataName = "";
            if (!result.PermataID.Empty())
            {
                var permata = _permataDal.GetData(result);
                permata.Empty().Throw("Permata ID invalid");
                result.PermataName = permata.PermataName;
            }

            return result;
        }

        #endregion
    }
}
