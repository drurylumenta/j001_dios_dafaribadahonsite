﻿using Dios.Lib.Model;
using Dios.Lib.Dal;
using System;
using System.Collections.Generic;
using System.Text;
using Nuna.Lib.Helper;
using Dios.Lib.Dto;

namespace Dios.Lib.BL
{

    public interface IJadwalBL
    {
        JadwalModel Add(JadwalAddDto jadwal);
        JadwalModel Update(IJadwalKey key, JadwalAddDto jadwal);
        void Delete(IJadwalKey jadwal);
        JadwalModel GetData(IJadwalKey jadwal);
        IEnumerable<JadwalModel> ListData();
    }

    public class JadwalBL : IJadwalBL
    {
        private readonly IIbadahTypeDal _ibadahTypeDal;
        private readonly IJadwalDal _jadwalDal;
        private readonly INunaCounterBL _nunaCounterBL;

        public JadwalBL(IJadwalDal jadwalDal,
            IIbadahTypeDal ibadahTypeDal,
            INunaCounterBL nunaCounterBL)
        {
            _jadwalDal = jadwalDal;
            _ibadahTypeDal = ibadahTypeDal;
            _nunaCounterBL = nunaCounterBL;
        }

        public JadwalModel Add(JadwalAddDto jadwal)
        {
            //      VALIDATE ARGUMENT
            var result = Validate(jadwal);

            //      APPLY REPO
            using (var trans = TransHelper.NewScope())
            {

                result.JadwalID = _nunaCounterBL.Generate("JI", IDFormatEnum.Prefix_3);
                _jadwalDal.Insert(result);
                trans.Complete();
            }

            //      SET RETURN
            return result;
        }

        public JadwalModel Update(IJadwalKey key, JadwalAddDto jadwal)
        {
            //      VALIDATE ARGUMENT
            var result = Validate(jadwal);
            result.JadwalID = key.JadwalID;
            var jad = _jadwalDal.GetData(result);
            jad.Empty().Throw("Jadwal ID invalid");

            //      APPLY REPO
            using (var trans = TransHelper.NewScope())
            {
                _jadwalDal.Update(result);
                trans.Complete();
            }

            //      SET RETURN
            return result;
        }

        public void Delete(IJadwalKey jadwal)
        {
            _jadwalDal.Delete(jadwal);
        }

        public JadwalModel GetData(IJadwalKey jadwal)
        {
            return _jadwalDal.GetData(jadwal);
        }

        public IEnumerable<JadwalModel> ListData()
        {
            return _jadwalDal.ListData();
        }



        private JadwalModel Validate(JadwalAddDto jadwal)
        {
            jadwal.Empty().Throw("Data Jadwal kosong");
            jadwal.JadwalName.Empty().Throw("Nama Jadwal kosong");
            jadwal.IbadahTypeID.Empty().Throw("Tipe Ibadah kosong");
            
            //  convert dto >> model
            var result = (JadwalModel)jadwal;

            //  advance validate
            var ibadahType = _ibadahTypeDal.GetData(result);
            ibadahType.Empty().Throw("Tipe Ibadah invalid");
            result.IbadahTypeName = ibadahType.IbadahTypeName;

            return result;
        }
    }
}
