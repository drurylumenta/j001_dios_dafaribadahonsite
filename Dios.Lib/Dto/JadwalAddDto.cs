﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Dto
{
    public class JadwalAddDto : INunaModel,
        IIbadahTypeKey
    {
        public string JadwalName { get; set; }
        public string IbadahTypeID { get; set; }
        public int Hari { get; set; }
        public string Jam { get; set; }

        public static explicit operator JadwalModel(JadwalAddDto dto)
        {
            dto.Empty().Throw("Data Jadwal kosong");
            if (!Enum.IsDefined(typeof(DayOfWeek), dto.Hari))
                throw new ArgumentException("Index Hari invalid");
            TimeSpan jamTS;
            if (!TimeSpan.TryParse(dto.Jam, out jamTS))
                throw new ArgumentException("Format Jam invalid");

            var result = new JadwalModel
            {
                JadwalID = "",
                JadwalName = dto.JadwalName,
                Hari = (DayOfWeek)dto.Hari,
                Jam = dto.Jam,
                IbadahTypeID = dto.IbadahTypeID,
                IbadahTypeName = "",
            };
            return result;
        }
    }
}
