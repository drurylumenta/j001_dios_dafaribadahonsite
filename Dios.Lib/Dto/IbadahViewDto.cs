﻿using Dios.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Dto
{
    public class IbadahViewDto :
        IIbadahKey, IIbadahTypeKey
    {
        public string IbadahID { get; set; }
        public string IbadahTypeID { get; set; }
        public string IbadahTypeName { get; set; }
        public string Tgl { get; set; }
        public string Jam { get; set; }
        public int Kapasitas { get; set; }
        public int Daftar { get; set; }

        public static explicit operator IbadahViewDto(IbadahModel model)
        {
            var result = new IbadahViewDto
            {
                IbadahID = model.IbadahID,
                IbadahTypeID = model.IbadahTypeID,
                IbadahTypeName = model.IbadahTypeName,
                Tgl = model.TglJam.ToString("yyyy-MM-dd"),
                Jam = model.TglJam.ToString("HH:mm"),
                Kapasitas = model.Kapasitas,
                Daftar = model.Daftar
            };
            return result;
        }

        public static IEnumerable<IbadahViewDto> Convert(IEnumerable<IbadahModel> models)
        {
            if (models is null) return null;
            var result = new List<IbadahViewDto>();
            foreach (var item in models)
                result.Add((IbadahViewDto)item);
            return result;
        }
    }
}
