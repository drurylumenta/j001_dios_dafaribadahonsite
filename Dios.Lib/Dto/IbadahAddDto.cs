﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Dto
{
    public class IbadahAddDto : INunaModel,
        IIbadahTypeKey
    {
        public string IbadahTypeID { get; set; }
        public string Tgl { get; set; }
        public string Jam { get; set; }
        public int Kapasitas { get; set; }

        public static explicit operator IbadahModel(IbadahAddDto dto)
        {
            if (dto is null) return null;
            var time = TimeSpan.Parse(dto.Jam);
            var result = new IbadahModel
            {
                IbadahTypeID = dto.IbadahTypeID,
                TglJam = dto.Tgl.ToDate().Add(time),
                Kapasitas = dto.Kapasitas,
                Daftar = 0,
                IbadahTypeName = "",
                IbadahID = "",
            };
            return result;
        }
    }
}
