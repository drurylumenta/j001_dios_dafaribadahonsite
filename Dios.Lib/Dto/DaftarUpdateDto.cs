﻿using Dios.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Dto
{
    public class DaftarUpdateDto : DaftarAddDto,
        IDaftarKey
    {
        public string DaftarID { get; set; }
    }
}
