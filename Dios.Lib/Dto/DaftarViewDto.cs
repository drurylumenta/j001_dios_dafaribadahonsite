﻿using Dios.Lib.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Dto
{
    public class DaftarViewDto :
        IDaftarKey, IIbadahKey, IPermataKey, 
        IUserrKey
    {
        public string DaftarID { get; set; }
        public string TglDaftar { get; set; }
        public string JamDaftar { get; set; }
        public string UserrID { get; set; }
        public string UserrName { get; set; }


        public string IbadahID { get; set; }
        public string IbadahTypeName { get; set; }
        public string TglIbadah { get; set; }
        public string JamIbadah { get; set; }


        public string PesertaName { get; set; }
        public int JumPerson { get; set; }
        public string Keterangan { get; set; }

        public string PermataID { get; set; }
        public string PermataName { get; set; }

        public static explicit operator DaftarViewDto(DaftarModel model)
        {
            var result = new DaftarViewDto
            {
                DaftarID = model.DaftarID,
                TglDaftar = model.TglJam.ToString("yyyy-MM-dd"),
                JamDaftar = model.TglJam.ToString("HH:mm:ss"),
                UserrID = model.UserrID,
                UserrName = model.UserrName,

                IbadahID = model.IbadahID,
                IbadahTypeName = model.IbadahTypeName,
                TglIbadah = model.TglJamIbadah.ToString("yyyy-MM-dd"),
                JamIbadah = model.TglJamIbadah.ToString("HH:mm"),

                PesertaName = model.PesertaName,
                JumPerson = model.JumPerson,
                Keterangan = model.Keterangan,
                PermataID = model.PermataID,
                PermataName = model.PermataName
            };
            return result;
        }

        public static IEnumerable<DaftarViewDto> Convert(IEnumerable<DaftarModel> models)
        {
            if (models is null) return null;
            var result = new List<DaftarViewDto>();
            foreach (var item in models)
                result.Add((DaftarViewDto)item);
            return result;
        }
    }
}
