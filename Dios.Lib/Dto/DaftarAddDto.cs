﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Dto
{
    public class DaftarAddDto : INunaModel,
        IIbadahKey, IPermataKey, 
        IUserrKey
    {
        public string UserrID { get; set; }

        public string IbadahID { get; set; }

        public string PesertaName { get; set; }
        public int JumPerson { get; set; }
        public string Keterangan { get; set; }

        public string PermataID { get; set; }

        public static explicit operator DaftarModel(DaftarAddDto dto)
        {
            var result = new DaftarModel
            {
                DaftarID = "",
                TglJam = NunaDateTime.Default,
                UserrID = dto.UserrID,
                UserrName = "",
                IbadahID = dto.IbadahID,
                IbadahTypeID = "",
                IbadahTypeName = "",
                TglJamIbadah = NunaDateTime.Default,
                PesertaName = dto.PesertaName,
                JumPerson = dto.JumPerson,
                Keterangan = dto.Keterangan,
                PermataID = dto.PermataID,
                PermataName = ""
            };
            return result;
        }
    }
}
