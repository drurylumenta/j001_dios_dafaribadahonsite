﻿using Dios.Lib.Model;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Dto
{
    public class IbadahUpdateDto : IbadahAddDto,
        IIbadahKey
    {
        public string IbadahID { get; set; }
    }
}
