﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib
{
    public static class ConnStringHelper
    {
        public static string Get()
        {
            var appSettings = new ConfigurationBuilder()
                .AddJsonFile("db-connection.json")
                .Build();
            var server = appSettings["Server"];
            var database = appSettings["Database"];
            var userID = "DiosLogin";
            var password = "Dios123";
            var result = string.Format(
                "Server={0};Database={1};User Id={2};Password = {3};",
                server, database, userID, password);
            return result;
        }
    }
}
