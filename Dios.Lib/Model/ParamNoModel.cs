﻿using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Model
{
    public interface IParamNoKey
    {
        string Prefix { get; set; }
    }
    public class ParamNoModel : INunaModel,
        IParamNoKey
    {
        public string Prefix { get; set; }
        public string PValue { get; set; }
    }
}
