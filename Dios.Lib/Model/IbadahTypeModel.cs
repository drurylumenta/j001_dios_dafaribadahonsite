﻿using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Model
{

    public interface IIbadahTypeKey
    {
        string IbadahTypeID { get; set; }
    }
    public class IbadahTypeModel : INunaModel,
        IIbadahTypeKey
    {
        public string IbadahTypeID { get; set; }
        public string IbadahTypeName { get; set; }
    }
}
