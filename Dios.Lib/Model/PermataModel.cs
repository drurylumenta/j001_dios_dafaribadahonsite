﻿using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Model
{

    public interface IPermataKey
    {
        string PermataID { get; set; }
    }
    public class PermataModel : INunaModel,
        IPermataKey
    {
        public string PermataID { get; set; }
        public string PermataName { get; set; }
    }
}
