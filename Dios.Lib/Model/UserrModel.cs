﻿using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Model
{

    public enum UserrLevelEnum
    {
        Jemaat, Admin
    }
    public interface IUserrKey
    {
        string UserrID { get; set; }
    }
    public class UserrModel : INunaModel,
        IUserrKey
    {
        public string UserrID { get; set; }
        public string UserrName { get; set; }
        public string DeviceToken { get; set; }
        public UserrLevelEnum UserrLevel { get; set; }
    }
}
