﻿using Dios.Lib.Dto;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Model
{

    public interface IIbadahKey
    {
        string IbadahID { get; set; }
    }
    public class IbadahModel : INunaModel,
        IIbadahKey, IIbadahTypeKey
    {
        public string IbadahID { get; set; }

        public string IbadahTypeID { get; set; }
        public string IbadahTypeName { get; set; }
        public DateTime TglJam { get; set; }
        public int Kapasitas { get; set; }
        public int Daftar { get; set; }

    }
}
