﻿using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Model
{

    public interface IJadwalKey
    {
        string JadwalID { get; set; }
    }
    public class JadwalModel : INunaModel,
        IJadwalKey, IIbadahTypeKey
    {
        public string JadwalID { get; set; }
        public string JadwalName { get; set; }

        public string IbadahTypeID { get; set; }
        public string IbadahTypeName { get; set; }

        public DayOfWeek Hari { get; set; }
        public string Jam { get; set; }
    }

}
