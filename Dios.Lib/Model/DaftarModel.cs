﻿using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dios.Lib.Model
{

    public interface IDaftarKey
    {
        string DaftarID { get; set; }
    }
    public class DaftarModel : INunaModel,
        IDaftarKey, IIbadahKey, IPermataKey, 
        IUserrKey, IIbadahTypeKey
    {
        public string DaftarID { get; set; }
        public DateTime TglJam { get; set; }
        public string UserrID { get; set; }
        public string UserrName { get; set; }


        public string IbadahID { get; set; }
        public string IbadahTypeID { get; set; }
        public string IbadahTypeName { get; set; }
        public DateTime TglJamIbadah { get; set; }


        public string PesertaName { get; set; }
        public int JumPerson { get; set; }
        public string Keterangan { get; set; }

        public string PermataID { get; set; }
        public string PermataName { get; set; }
    }
}
