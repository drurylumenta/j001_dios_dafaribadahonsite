using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dios.Lib.BL;
using Dios.Lib.Dal;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nuna.Lib.Helper;

namespace Dios.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin",
                    builder =>
                    {
                        // insecure !!!
                        builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });
            services.AddControllers();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v2", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Birawa API",
                    Version = "v2",
                    Description = "Billing Registrasi Service",
                });
            });
            

            services.AddScoped<IUserrBL, UserrBL>();
            services.AddScoped<IUserrDal, UserrDal>();
            
            services.AddScoped<IIbadahTypeBL, IbadahTypeBL>();
            services.AddScoped<IIbadahTypeDal, IbadahTypeDal>();

            services.AddScoped<IJadwalBL, JadwalBL>();
            services.AddScoped<IJadwalDal, JadwalDal>();

            services.AddScoped<IIbadahBL, IbadahBL>();
            services.AddScoped<IIbadahDal, IbadahDal>();

            services.AddScoped<IDaftarBL, DaftarBL>();
            services.AddScoped<IDaftarDal, DaftarDal>();

            services.AddScoped<IPermataDal, PermataDal>();
            services.AddScoped<IPermataBL, PermataBL>();

            services.AddScoped<INunaCounterBL, NunaCounterBL>();
            services.AddScoped<INunaCounterDal, ParamNoDal>();
            services.AddScoped<IParamNoDal, ParamNoDal>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("AllowOrigin");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action}/{id?}");
            });

            app.UseSwagger();

            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v2/swagger.json", "PlaceInfo Services"));
        }
    }
}
