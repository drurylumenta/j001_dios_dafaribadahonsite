﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dios.Lib.BL;
using Dios.Lib.Dto;
using Dios.Lib.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nuna.Lib.Helper;

namespace Dios.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class DaftarController : ControllerBase
    {
        private readonly IDaftarBL _daftarBL;

        public DaftarController(IDaftarBL daftarBL)
        {
            _daftarBL = daftarBL;
        }

        [HttpPost]
        public ActionResult<DaftarViewDto> Add(DaftarAddDto daftar)
        {
            try
            {
                var result = _daftarBL.Add(daftar);
                return Ok((DaftarViewDto)result);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpPut]
        public ActionResult<DaftarViewDto> Update(DaftarUpdateDto daftar)
        {
            try
            {
                var result = _daftarBL.Update(daftar);
                return Ok((DaftarViewDto)result);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var daf = new DaftarModel { DaftarID = id };
            try
            {
                _daftarBL.Delete(daf);
                return Ok();
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<DaftarModel> GetData(string id)
        {
            var daf = new DaftarModel { DaftarID = id };
            try
            {
                var result = _daftarBL.GetData(daf);
                if (result != null)
                    return Ok((DaftarViewDto)result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<DaftarModel>> ListData(string ibadahID)
        {
            var ibadah = new IbadahModel { IbadahID = ibadahID };
            try
            {
                var result = _daftarBL.ListData(ibadah);
                if (result != null)
                    return Ok(DaftarViewDto.Convert(result));
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }
    }

}
