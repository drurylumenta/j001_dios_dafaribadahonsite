﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dios.Lib.BL;
using Dios.Lib.Dto;
using Dios.Lib.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nuna.Lib.Helper;

namespace Dios.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class JadwalController : ControllerBase
    {
        private readonly IJadwalBL _jadwalBL;

        public JadwalController(IJadwalBL jadwalBL)
        {
            _jadwalBL = jadwalBL;
        }

        [HttpPost]
        public ActionResult<JadwalModel> Add(JadwalAddDto jadwal)
        {
            try
            {
                var result = _jadwalBL.Add(jadwal);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpPut]
        public ActionResult<JadwalModel> Update(string id, JadwalAddDto jadwal)
        {
            var key = new JadwalModel { JadwalID = id };
            try
            {
                var result = _jadwalBL.Update(key, jadwal);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }


        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var jad = new JadwalModel { JadwalID = id };
            try
            {
                _jadwalBL.Delete(jad);
                return Ok();
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<JadwalModel> GetData(string id)
        {
            var jad = new JadwalModel { JadwalID = id };
            try
            {
                var result = _jadwalBL.GetData(jad);
                if (result != null)
                    return Ok(result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<JadwalModel>> ListData()
        {
            try
            {
                var result = _jadwalBL.ListData();
                if (result != null)
                    return Ok(result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }
    }
}
