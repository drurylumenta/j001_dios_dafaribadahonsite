﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dios.Lib.BL;
using Dios.Lib.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nuna.Lib.Helper;

namespace Dios.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class IbadahTypeController : ControllerBase
    {
        private readonly IIbadahTypeBL _ibadahTypeBL;

        public IbadahTypeController(IIbadahTypeBL ibadahTypeBL)
        {
            _ibadahTypeBL = ibadahTypeBL;
        }

        [HttpPost]
        public ActionResult<IbadahTypeModel> Save(IbadahTypeModel ibadahType)
        {
            try
            {
                var result = _ibadahTypeBL.Save(ibadahType);
                return Ok(ibadahType);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var it = new IbadahTypeModel { IbadahTypeID = id };
            try
            {
                _ibadahTypeBL.Delete(it);
                return Ok();
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<IbadahTypeModel> GetData(string id)
        {
            var it = new IbadahTypeModel { IbadahTypeID = id };
            try
            {
                var result = _ibadahTypeBL.GetData(it);
                if (result != null)
                    return Ok(result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<IbadahTypeModel>> ListData()
        {
            try
            {
                var result = _ibadahTypeBL.ListData();
                if (result != null)
                    return Ok(result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }
    }
}
