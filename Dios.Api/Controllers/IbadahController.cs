﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dios.Lib.BL;
using Dios.Lib.Dto;
using Dios.Lib.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nuna.Lib.Helper;

namespace Dios.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class IbadahController : ControllerBase
    {
        private readonly IIbadahBL _ibadahBL;

        public IbadahController(IIbadahBL ibadahBL)
        {
            _ibadahBL = ibadahBL;
        }

        [HttpPost]
        public ActionResult<IbadahViewDto> Add(IbadahAddDto ibadah)
        {
            try
            {
                var result = _ibadahBL.Add(ibadah);
                return Ok((IbadahViewDto)result);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpPut]
        public ActionResult<IbadahViewDto> Update(IbadahUpdateDto ibadah)
        {
            try
            {
                var result = _ibadahBL.Update(ibadah);
                return Ok((IbadahViewDto)result);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var ibd = new IbadahModel { IbadahID = id };
            try
            {
                _ibadahBL.Delete(ibd);
                return Ok();
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<IbadahViewDto> GetData(string id)
        {
            var ibd = new IbadahModel { IbadahID = id };
            try
            {
                var result = _ibadahBL.GetData(ibd);
                if (result != null)
                    return Ok((IbadahViewDto)result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<IbadahViewDto>> ListData(string tglYmd1, string tglYmd2)
        {
            var periode = new NunaPeriode(tglYmd1.ToDate(), tglYmd2.ToDate());
            try
            {
                var result = _ibadahBL.ListData(periode);
                if (result != null)
                    return Ok(IbadahViewDto.Convert(result));
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }
    }

}
