﻿using Dios.Lib.BL;
using Dios.Lib.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Nuna.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dios.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class UserrController : ControllerBase
    {
        private readonly IUserrBL _userrBL;

        public UserrController(IUserrBL userrBL)
        {
            _userrBL = userrBL;
        }

        [HttpPost]
        public ActionResult<UserrModel> Save(UserrModel userr)
        {
            try
            {
                var result = _userrBL.Save(userr);
                return Ok(userr);
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var us = new UserrModel { UserrID = id };
            try
            {
                _userrBL.Delete(us);
                return Ok();
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<UserrModel> GetData(string id)
        {
            var us = new UserrModel { UserrID = id };
            try
            {
                var result = _userrBL.GetData(us);
                if (result != null)
                    return Ok(result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserrModel>> ListData()
        {
            try
            {
                var result = _userrBL.ListData();
                if (result != null)
                    return Ok(result);
                else
                    throw new ArgumentException("Data Not Found");
            }
            catch (Exception ex)
            {
                var nuna = new NunaErrorHandle(ex);
                return StatusCode(nuna.StatusCode, nuna.ErrObj);
            }
        }
    }

}
