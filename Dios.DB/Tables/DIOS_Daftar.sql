﻿CREATE TABLE DIOS_Daftar
(
	DaftarID VARCHAR(15) NOT NULL CONSTRAINT DF_DIOS_Daftar_DaftarID DEFAULT(''),
	TglJam DATETIME NOT NULL CONSTRAINT DF_DIOS_Daftar_TglJam DEFAULT('1900-01-01T00:00:00'),
	UserrID VARCHAR(128) NOT NULL CONSTRAINT DF_DIOS_Daftar_UserrID DEFAULT(''),

	IbadahID VARCHAR(11) NOT NULL CONSTRAINT DF_DIOS_Daftar_IbadahID DEFAULT(''),
	PesertaName VARCHAR(50) NOT NULL CONSTRAINT DF_DIOS_Daftar_KeluargaName DEFAULT(''),
	JumPerson INT NOT NULL CONSTRAINT DF_DIOS_Daftar_JumPerson DEFAULT(0),
	Keterangan VARCHAR(128) NOT NULL CONSTRAINT DF_DIOS_Daftar_Keterangan DEFAULT(''),

	PermataID VARCHAR(5) NOT NULL CONSTRAINT DF_DIOS_Daftar_PermataID DEFAULT(''),

	CONSTRAINT PK_DIOS_Daftar_DaftarID PRIMARY KEY CLUSTERED (DaftarID)
)
